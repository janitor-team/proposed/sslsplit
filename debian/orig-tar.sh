#!/bin/sh

set -e

VERSION=$2
FILE=$3

bzcat $FILE | tar --wildcards --delete '*/xnu' | bzip2 -c > ../sslsplit_$VERSION+dfsg.orig.tar.bz2.t
mv ../sslsplit_$VERSION+dfsg.orig.tar.bz2.t \
   ../sslsplit_$VERSION+dfsg.orig.tar.bz2

